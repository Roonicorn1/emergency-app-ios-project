//
//  Alerts.swift
//  IOSFinalProject
//
//  Created by Student on 3/11/19.
//  Copyright © 2019 student. All rights reserved.
//

import Foundation
struct Alert{
    var title:String
    var alertMessages:String
    public init(title: String, alertMessages: String){
        self.title = title
        self.alertMessages = alertMessages
    }
    
}

struct Alerts{
    static var alerts = Alerts()
    static var shared = Alerts()
    
    private var alerts:[Alert] = [Alert(title: "Weather", alertMessages: "There is storm"), Alert(title: "Burglary", alertMessages: "There are thievs in my house")]

    func NumAlertMessages()->Int {
        return alerts.count
    }
    
    func alertMessages(_ index:Int) -> Alert {
        return alerts[index]
    }
    mutating func add(alert:Alert){
        alerts.append(alert)
    }
    
    subscript(index:Int) -> Alert {
        return alerts[index]
    }
        
}

