//
//  ContactsTableViewController.swift
//  IOSFinalProject
//
//  Created by Student on 3/11/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class ContactsTableViewController: UITableViewController {
    private weak var button: UIButton!
    @IBOutlet weak var searchBar: UITableView!
    
    static var phoneNumber:[String] = []
    var contactsSelected = DataManager.shared.contacts
    var contacts = DataManager.shared.contacts
    var swipedIndex: Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        DataManager.shared.loadContacts()
        contacts = DataManager.shared.contacts
        tableView.reloadData()
        
    }
    @objc func contactsRetrieved() {
        DispatchQueue.main.sync {
            DataManager.shared.loadContacts()
            contacts = DataManager.shared.contacts
            self.tableView.reloadData()
        }
    }
    var searchedContact = [String]()
    var searching = false
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        DataManager.shared.loadContacts()
        contacts = DataManager.shared.contacts
        tableView.reloadData()
    }
    
    //Function to have delete a row
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete"){
            (action, View, nil) in DataManager.shared.delete(phone: DataManager.shared.getContacts()[indexPath.row].phoneNumber)
            
            DataManager.shared.loadContacts()
            self.contacts = DataManager.shared.contacts
            self.tableView.reloadData()
        }
        delete.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        self.tableView.reloadData()
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    //function to have edit a row
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit = UIContextualAction(style: .normal, title: "Edit"){(action, view, nil) in
            self.swipedIndex = indexPath.row
            self.performSegue(withIdentifier: "editContact", sender: nil)
        } //EditContactViewController().viewDidLoad()}
        DataManager.shared.loadContacts()
        self.contacts = DataManager.shared.contacts
        self.tableView.reloadData()
        edit.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        return UISwipeActionsConfiguration(actions: [edit])
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching{
            return searchedContact.count
        }
        return contacts.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCells")
        if searching{
            cell?.textLabel?.text = searchedContact[indexPath.row]
            
        }
        else {
            cell?.textLabel?.text = "\(contacts[indexPath.row].lastName!), \(contacts[indexPath.row].firstName!)"
        }
        return cell!
    }
    var selectedContacts:[IndexPath:ContactsData] = [:]
    var checkedFlag:Int = 0
    //Function for multi-select functionality
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark{
                cell.accessoryType = .none
                selectedContacts.removeValue(forKey: indexPath)
                checkedFlag = checkedFlag - 1
            }
            else{
                cell.accessoryType = .checkmark
                selectedContacts[indexPath] = contacts[indexPath.row]
                checkedFlag = checkedFlag + 1
                for i in 0..<selectedContacts.count{
                    ContactsTableViewController.phoneNumber.append(String(contactsSelected[i].phoneNumber))
                }
            }
        }
    }
    //This function shows if send button is clicked without selecting any contact
    @IBAction func selectAlerts(_ sender: Any) {
        if (checkedFlag != 0){
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AlertViewController") as! AlertViewController
            navigationController?.pushViewController(nextViewController, animated: true)
        }
        else{
            let alert = UIAlertController(title: "Please select a Contact", message: "Please select atleast one contact", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "AlertMessages"{
            if (checkedFlag != 0){
                let alertsVC = segue.destination as! AlertViewController
                if let indexes = tableView.indexPathsForSelectedRows{
                    for indexPaths in indexes{
                        alertsVC.contactSelected.append(DataManager.shared.getContacts()[indexPaths.row])
                    }
                }
            }
            else{
                print("NAH")
            }
        }
        //prepare segue for the edit contact view controller
        if segue.identifier == "editContact"{
            let editContactVC = segue.destination as! EditContactViewController
            editContactVC.editContact = DataManager.shared.getContacts()[self.swipedIndex]//[tableView.indexPathForSelectedRow!.row]
        }
    }
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "save"
        {
            return selectedContacts.count > 0
        }
        else{
            return true
        }
    }
    @IBAction func cancelAddingNewContact(unwindSegue: UIStoryboardSegue){
    }
    
    @IBAction func doneAddingNewContact(unwindSegue: UIStoryboardSegue){
        self.tableView.reloadData()
    }
}
extension ContactsTableViewController: UISearchBarDelegate {
    //Function for searching among Contacts
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchedContact = DataManager.shared.returnContactsFirstName().filter({$0.lowercased().prefix(searchText.count) == searchText.lowercased()})
        //        print(searchedContact)
        searching = true
        self.tableView.reloadData()
    }
    //Function for cancel button on search Bar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searching = false
        searchBar.text = ""
        self.tableView.reloadData()
    }
}
