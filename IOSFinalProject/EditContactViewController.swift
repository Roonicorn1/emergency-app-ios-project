//
//  EditContactViewController.swift
//  IOSFinalProject
//
//  Created by student on 4/4/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class EditContactViewController: UIViewController
{
    var editContact: ContactsData!
    //This function edits the selected contact
    @IBAction func DoneEditing(_ sender: Any) {
        let firstName = firstNameTF.text
        let lastName = lastNameTF.text
        let phoneNo = Int64(phoneNumberTF.text!)
        if let firstName = firstName , let lastName = lastName, let phoneNo = phoneNo{
            DataManager.shared.addContact(first: firstName, last: lastName, phone: phoneNo)
            DataManager.shared.delete(phone: editContact.phoneNumber)
            DataManager.shared.loadContacts()
            self.dismiss(animated: false, completion: nil)
        }
        else{
            DataManager.shared.addAlert(mes: "Please enter valid contact details", tit: "Error")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        firstNameTF.text = editContact.firstName
        lastNameTF.text = editContact.lastName
        phoneNumberTF.text = String(editContact.phoneNumber)
    }
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBAction func CancelEditing(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
