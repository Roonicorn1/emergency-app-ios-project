//
//  Person.swift
//  IOSFinalProject
//
//  Created by Student on 3/11/19.
//  Copyright © 2019 student. All rights reserved.
//

import Foundation
struct Contact{
    let firstName:String
    let lastName:String
    let phoneNumber: Double
    
}
struct Contacts{
    static var shared = Contacts()
    
    private var contacts:[Contact] = []
    
    func getContacts() -> [Contact]{
        return Contacts.shared.contacts
    }
    
    subscript(index: Int) -> Contact{
    return Contacts.shared.contacts[index]
    }
    
    func returnContactsFirstName() -> [String]{
        var firstNameArray: [String] = []
        for i in Contacts.shared.contacts{
            firstNameArray.append(i.firstName)
        }
        return firstNameArray
    }
    
    func numContacts()->Int {
        return contacts.count
    }
    
    func contact(_ index:Int) -> Contact {
        return contacts[index]
    }
    mutating func add(contact:Contact){
        contacts.append(contact)
    }
    
    mutating func delete(contact:Contact){
        for i in 0 ..< contacts.count {
            if contacts[i].firstName == contact.firstName && contacts[i].lastName == contact.lastName {
                contacts.remove(at:i)
                break
            }
        }
        
    }

}
