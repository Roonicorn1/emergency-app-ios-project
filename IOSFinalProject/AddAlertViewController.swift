//
//  AddAlertViewController.swift
//  IOSFinalProject
//
//  Created by Student on 3/12/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class AddAlertViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var titleTF: UITextField!
    
    @IBOutlet weak var alertMessageTF: UITextField!
    
    @IBAction func DoneAddingAlertBTN(_ sender: Any) {
        if let title = titleTF.text , let message = alertMessageTF.text{
            if(title != "" && message != ""){
                DataManager.shared.addAlert(mes: message, tit: title)
                DataManager.shared.loadAlerts()
                self.dismiss(animated: false, completion: nil)
            }
            else{
                displayError()
            }
        }else{
            displayError()
        }
    }
    @IBAction func CancelAddingNewAlertBTN(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("segue: ", segue.identifier as Any)
        if segue.identifier == "doneAddingAlert" {
            if let title = titleTF.text , let message = alertMessageTF.text{
                if(title != "" && message != ""){
                    let alert = Alert(title: title, alertMessages: message)
                    Alerts.shared.add(alert: alert)
                    DataManager.shared.addAlert(mes: message, tit: title)
                }
                else{
                    displayError()
                }
            }else{
                displayError()
            }
        }
    }
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool{
        if identifier == "doneAddingAlert" {
            if let title = titleTF.text , let message = alertMessageTF.text{
                if(title != "" && message != ""){
                    return true
                }
                else{
                    displayError()
                }
            }
            return false
        }
        return true
    }
    //Function for AlertBox which popups if values are null
    func displayError(){
        let alertController = UIAlertController(title: "ERROR", message: "Empty values are not allowed", preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    
}
