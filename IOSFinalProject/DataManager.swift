//
//  File.swift
//  IOSFinalProject
//
//  Created by student on 4/17/19.
//  Copyright © 2019 student. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class DataManager {
    
    static var shared = DataManager()
    var controller: NSFetchedResultsController<ContactsData>!
    var context:NSManagedObjectContext!
    var appDelegate:AppDelegate!
    var contacts:[ContactsData] = []
    var alerts: [AlertsData] = []
    
    private init(){
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        context = appDelegate.persistentContainer.viewContext
        loadContacts()
        loadAlerts()
    }
    
    func addContact(first:String, last:String, phone:Int64){
        let newContact = NSEntityDescription.insertNewObject(forEntityName: "ContactsData",   into: context) as! ContactsData
        newContact.firstName = first
        newContact.lastName = last
        newContact.phoneNumber = phone
        
        appDelegate.saveContext()
        loadContacts()
    }
    func returnContactsFirstName() -> [String]{
        var nameArray: [String] = []
        for i in contacts{
            nameArray.append(i.firstName!+", "+i.lastName!)
        }
        return nameArray
    }
    
    func getContacts() -> [ContactsData]{
        return contacts
    }
    
    func returnAlerts() -> [String]{
        var alertArray:[String] = []
        for i in alerts{
            alertArray.append(i.title!)
        }
        return alertArray
    }
    
    func loadContacts(){
        cleanData()
        
        let request:NSFetchRequest<ContactsData> = NSFetchRequest(entityName: "ContactsData")
        // fetch the data into an array, countries - if it fails, the if is skipped
        if let data = try? context.fetch(request) {
            self.contacts = data
        }
    }
    func delete(phone:Int64){
        // to delete a contact, first fetch it ...
        let request:NSFetchRequest<ContactsData> = NSFetchRequest(entityName: "ContactsData")
        if let results = try? context.fetch(request) {
            if results.count >= 0 {
                for i in 0..<contacts.count{
                    if contacts[i].phoneNumber == phone {
                        context.delete(results[i] as NSManagedObject) // then delete it
                        contacts.remove(at:i)
                        appDelegate.saveContext() // don't forget this!!!
                        print("Deleted: "+String(phone))
                        break
                    }
                }
            }
        }
        
    }
    func cleanData(){
        // to delete a contact, first fetch it ...
        let request:NSFetchRequest<ContactsData> = NSFetchRequest(entityName: "ContactsData")
        
        if let results = try? context.fetch(request) {
            for c in results {
                if c.firstName == nil || c.lastName == nil || c.phoneNumber == 0{
                    context.delete(c as NSManagedObject) // then delete it
                }
            appDelegate.saveContext()
            }
        }
        
    }
   
    
    func addAlert(mes:String , tit:String){
        let newContact = NSEntityDescription.insertNewObject(forEntityName: "AlertsData",   into: context) as! AlertsData
        newContact.message = mes
        newContact.title = tit
        
        appDelegate.saveContext()
        
        loadAlerts()
    }
    
    
    func loadAlerts(){
        let request:NSFetchRequest<AlertsData> = NSFetchRequest(entityName: "AlertsData")
        
        if let data = try? context.fetch(request) {
            self.alerts = data
        }
    }
    func cleanAlertsData(){
        // to delete a country, first fetch it ...
        let request:NSFetchRequest<AlertsData> = NSFetchRequest(entityName: "AlertsData")
        
        if let results = try? context.fetch(request) {
            for contents in results {
                if contents.message == nil || contents.title == nil {
                    context.delete(contents as NSManagedObject) // then delete it
                }
                appDelegate.saveContext()
            }
        }
        
    }
    func deleteAlert(t : String){
        // to delete a country, first fetch it ...
        let request:NSFetchRequest<AlertsData> = NSFetchRequest(entityName: "AlertsData")
        
        if let results = try? context.fetch(request) {
            if results.count >= 0 {
                for i in 0..<alerts.count{
                    if alerts[i].title == t {
                        context.delete(results[i] as NSManagedObject) // then delete it
                        alerts.remove(at:i)
                        appDelegate.saveContext() 
                        break
                    }
                }
            }
        }
        
    }
    func getAlerts() -> [AlertsData]{
        return alerts
    }
    

}
