//
//  NewContactViewController.swift
//  IOSFinalProject
//
//  Created by Student on 3/12/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class NewContactViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var firstNameTF: UITextField!
    
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "done" {
            let pn = Int(phoneNumberTF.text!)
            if let firstname = firstNameTF.text , let lastname = lastNameTF.text{
                if (firstname != "" && lastname != ""){
                if let phonenumber = phoneNumberTF.text{
                    if phonenumber.count == 10 && pn != nil{
                        let phn = Int(phonenumber)
                        let addedContact = Contact(firstName: firstname, lastName: lastname, phoneNumber: Double(phn!))
                        Contacts.shared.add(contact: addedContact)
                        DataManager.shared.addContact(first: firstname, last: lastname, phone: Int64(phn!))
//                        DataManager.shared.loadContacts()
                    }
                    else{
                        displayErrorRangeNumber()
                    }
                    
                }else{
                    displayErrorRangeNumber()
                }
            }
                else{
                    displayError()
                }
            }
            else{
                displayError()
            }
            
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "done" {
            let pn = Int(phoneNumberTF.text!)
            if let firstname = firstNameTF.text , let lastname = lastNameTF.text{
                if (firstname != "" && lastname != ""){
                    if let phonenumber = phoneNumberTF.text{
                        if phonenumber.count == 10 && pn != nil{
                            let phn = Int(phonenumber)
                            _ = Contact(firstName: firstname, lastName: lastname, phoneNumber: Double(phn!))
                            return true
                        }
                        else{
                            displayErrorRangeNumber()
                        }
                        
                    }else{
                        displayErrorRangeNumber()
                    }
                }
                else{
                    displayError()
                }
            }
            else{
                displayError()
            }
            return false
        }
        return true
    }
    //This function displays error When firstname and lastname textfileds are given any numeric values instead of String
    func displayError(){
        let alertController = UIAlertController(title: "ERROR", message: "Enter name as a string", preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    //This function displays error when number is less or greater than 10 digits
    func displayErrorRangeNumber(){
        let alertController = UIAlertController(title: "ERROR", message: "Phone number should have 10 digits", preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }

}
