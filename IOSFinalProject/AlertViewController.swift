//
//  AlertViewController.swift
//  IOSFinalProject
//
//  Created by Student on 4/18/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit
import MessageUI
//MFMessageComposeViewControllerDelegate is for message Sending functionality
class AlertViewController: UIViewController, UISearchBarDelegate, MFMessageComposeViewControllerDelegate, UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return searchedAlert.count
        }
        return DataManager.shared.alerts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlertMessages", for: indexPath)
        self.alertts = DataManager.shared.alerts
        cell.textLabel?.text = alertts[indexPath.row].title
        print(alertts[indexPath.row].title as Any)
        return cell
    }
    
    var alerts:[String] = []
    var alertts:[AlertsData] = []
    var contactSelected: [ContactsData] = DataManager.shared.contacts
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    @IBOutlet var tableView: UITableView!  =  UITableView()
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidAppear(_ animated: Bool) {
        DataManager.shared.loadAlerts()
        alertts = DataManager.shared.alerts
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DataManager.shared.loadAlerts()
        self.alertts = DataManager.shared.alerts
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataManager.shared.loadAlerts()
        tableView.delegate = self
        tableView.dataSource = self
        self.alertts = DataManager.shared.alerts
        searchBar.delegate = self
        self.tableView.reloadData()
        
    }
    
    @IBAction func SendMessageBTN(_ sender: Any) {
        if MFMessageComposeViewController.canSendText() {
            print("SMS services are available")
            let controller = MFMessageComposeViewController()
            //        print("messageeeeL", message)
            controller.body = message
            controller.recipients = ContactsTableViewController.phoneNumber
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }else{
            print("SMS unavailable")
        }
        func messageComposeViewController(controller: MFMessageComposeViewController,
                                          didFinishWithResult result: MessageComposeResult) {
            controller.dismiss(animated: true, completion: nil)}
    
    }
    var message = ""
    var searchedAlert = [String]()
    var isSearching = false
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //Function for deleting rows
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete"){
            (action, View, nil) in DataManager.shared.deleteAlert(t: DataManager.shared.getAlerts()[indexPath.row].title!)
            DataManager.shared.loadAlerts()
            self.alertts = DataManager.shared.alerts
            print(self.alertts)
            self.tableView.reloadData()
        }
        delete.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        self.tableView.reloadData()
        return UISwipeActionsConfiguration(actions: [delete])
    }
    var selectedAlerts:[IndexPath:AlertsData] = [:]
    var checkedFlag = 0
    
    //Function for multi-Selection of rows
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        message = DataManager.shared.alerts[indexPath.row].message!
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark{
                cell.accessoryType = .none
                selectedAlerts.removeValue(forKey: indexPath)
                checkedFlag -= 1
            }
            else{
                cell.accessoryType = .checkmark
                selectedAlerts[indexPath] = DataManager.shared.alerts[indexPath.row]
                checkedFlag += 1
            }
        }
        
    }
    //Function for Searching among Alerts
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchedAlert = DataManager.shared.returnAlerts().filter({$0.lowercased().prefix(searchText.count) == searchText.lowercased()})
        isSearching = true
        tableView.reloadData()

    }
    //Function for Cancel button on search Bar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        searchBar.text = ""
        tableView.reloadData()
    }
    //Function for sending a SMS to mobile phone
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
}

