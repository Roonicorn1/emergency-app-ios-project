
# Project404

## Team members
- Andrew Hoskins
- Poojitha Singam
- Razeena Rao Nemarugommula
- Abhiram Madugula


## Overview
This app is mainly focused on sending Alerts as text messages to your familyMembers or Friends if you are struck in a catastrophic situation.
## Details
1. Project 404 allows you send Alerts to your selected contacts in the form of message.
2. It allows you create a new Alert or use predefined Alerts.

## Specifications
### Contacts Page
On opening the App, User will be navigated to this contact page
### Alerts Page
On clicking this, We can see all the alerts
### About
This tab contains information about Project404

## UX
![UX](UI.jpg )

## Duties
- Andrew Hoskins worked on Send functionality
- Poojitha Singam worked on Alerts Page.
- Razeena Rao Nemarugommula worked on Contacts Page.
- Abhiram Madugula worked on Editing and Deleting and Validation.

